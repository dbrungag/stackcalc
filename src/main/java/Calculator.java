import java.lang.reflect.InvocationTargetException;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.lang.*;
import java.lang.reflect.Constructor;

public class Calculator {
        private Scanner inStr;
        public Calculator(Scanner strType)
        {
            inStr = strType;
        }
        public Operation getOperation(String moduleDef) {
            Class cls;
            String opName, opParams;
            Scanner inputDef, parameterStr;
            Operation op = null;
            inputDef = new Scanner(moduleDef);
            opName = inputDef.next();
            if (inputDef.hasNextLine()) {
                opParams = inputDef.nextLine();
                parameterStr = new Scanner(opParams);
                try {
                    cls = Class.forName(opName);
                    Constructor[] constrs = cls.getConstructors();
                    Class[] params = constrs[0].getParameterTypes();
                    Constructor constr = cls.getConstructor(params);
                    op = (Operation) constr.newInstance(parameterStr);
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException exc) {
                    System.out.println(exc.toString());
                }
            }
            else {
                try {
                    cls = Class.forName(opName);
                    op = (Operation) cls.newInstance();
                } catch (ClassNotFoundException | InstantiationException | IllegalAccessException exc){
                    System.out.println(exc.toString());
                }
            }
            return op;
        }

        public void runOperations(CalcStack stack) throws PopEmptyStackException {
            String buf;
            Operation currentOp;
            while (inStr.hasNextLine()) {
                buf = inStr.nextLine();
                if (buf.charAt(0) == '#')
                    continue;
                currentOp = getOperation(buf);
                currentOp.doOperation(stack);
            }
        }
}
