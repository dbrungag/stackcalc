import java.util.HashMap;

public class CalcStack {
    private int size;
    private int capacity;
    private double[] content;
    private HashMap <String, Double> definitionList;
    public CalcStack() {
        size = 0;
        capacity = 64;
        content = new double[capacity];
        definitionList = new HashMap<String, Double>();
    }
    void addDef(String name, double value)
    {
        definitionList.put(name, value);
    }
    void push(double elem) {
        content[size] = elem;
        size++;
    }
    void push(String name) {
        content[size] = definitionList.get(name);
        size++;
    }
    double pop() throws PopEmptyStackException{
        size--;
        if (size < 0)
            throw new PopEmptyStackException();
        return content[size];
    }
    double top()
    {
        return content[size-1];
    }
}
