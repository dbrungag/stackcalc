import java.io.File;
import java.io.FileReader;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Calculator calculator;
        CalcStack stack;
        Scanner inStr;
        try {
            if (args.length == 0) {
                inStr = new Scanner(System.in);
            }
            else {
                if (args.length > 1) {
                    throw new CalcParamException();
                }
                FileReader reader = new FileReader(new File(args[0]));
                inStr = new Scanner(reader);
            }
            calculator = new Calculator(inStr);
            stack = new CalcStack();
            calculator.runOperations(stack);
        } catch (Exception exc) {
            System.out.println(exc.toString());
        }
    }
}
