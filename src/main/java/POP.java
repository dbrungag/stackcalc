public class POP implements Operation{
    public void doOperation(CalcStack stack) throws PopEmptyStackException{
        stack.pop();
    }
}
