public class SQRT implements Operation{
    public void doOperation(CalcStack stack) throws PopEmptyStackException{
        double operand = stack.pop();
        stack.push(Math.sqrt(operand));
    }
}
