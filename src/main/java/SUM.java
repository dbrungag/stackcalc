public class SUM implements Operation{
    public void doOperation(CalcStack stack) throws PopEmptyStackException{
        double  operand1 = stack.pop(),
                operand2 = stack.pop();
        stack.push(operand2 + operand1);
    }
}
