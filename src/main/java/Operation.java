public interface Operation {
    void doOperation(CalcStack stack) throws PopEmptyStackException;
}
