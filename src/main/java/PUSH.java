import java.util.Scanner;

public class PUSH implements Operation{
    private Scanner inStr;
    public PUSH(Scanner str)
    {
        inStr = str;
    }
    public void doOperation(CalcStack stack) {
        if (inStr.hasNextDouble())
            stack.push(inStr.nextDouble());
        else
            stack.push(inStr.next());
    }
}
