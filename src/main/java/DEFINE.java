import java.util.Scanner;

public class DEFINE implements Operation{
    private Scanner inStr;
    public DEFINE(Scanner str) {
        inStr = str;
    }
    public void doOperation(CalcStack stack) {
        double value;
        String elemName;
        elemName = inStr.next();
        value = inStr.nextDouble();
        stack.addDef(elemName, value);
    }
}
